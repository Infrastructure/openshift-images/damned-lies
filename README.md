# `openshift-images/damned-lies`

This project was originaly designed to build containers images to deploy the [`damned-lies`](https://gitlab.gnome.org/Infrastructure/damned-lies) application. To update image configurations, go to the `damned-lies` project.

Such containers are built from the `damned-lies` project itself through its own Contrinuous Integration. This project is kept alive in case.
